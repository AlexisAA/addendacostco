﻿namespace AddendaB1
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.lblTransaccion = new System.Windows.Forms.Label();
            this.lblPedido = new System.Windows.Forms.Label();
            this.lblAlbaran = new System.Windows.Forms.Label();
            this.txtDato1 = new System.Windows.Forms.TextBox();
            this.txtDato2 = new System.Windows.Forms.TextBox();
            this.txtDato3 = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lblAyuda = new System.Windows.Forms.Label();
            this.lblTipoDeProveedor = new System.Windows.Forms.Label();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.lblNoProveedor = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtDato4 = new System.Windows.Forms.TextBox();
            this.lblCorreo = new System.Windows.Forms.Label();
            this.txtDato5 = new System.Windows.Forms.TextBox();
            this.lblProveedor = new System.Windows.Forms.Label();
            this.lblPedidoP = new System.Windows.Forms.Label();
            this.lblRecepcion = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // textBox1
            // 
            this.textBox1.Enabled = false;
            this.textBox1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(148, 181);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(171, 23);
            this.textBox1.TabIndex = 1;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Tahoma", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(139, 210);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(191, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Seleccionar Archivo XML";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // lblTransaccion
            // 
            this.lblTransaccion.AutoSize = true;
            this.lblTransaccion.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTransaccion.Location = new System.Drawing.Point(13, 71);
            this.lblTransaccion.Name = "lblTransaccion";
            this.lblTransaccion.Size = new System.Drawing.Size(85, 16);
            this.lblTransaccion.TabIndex = 3;
            this.lblTransaccion.Text = "Transacción";
            this.lblTransaccion.Click += new System.EventHandler(this.label1_Click);
            // 
            // lblPedido
            // 
            this.lblPedido.AutoSize = true;
            this.lblPedido.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPedido.Location = new System.Drawing.Point(13, 108);
            this.lblPedido.Name = "lblPedido";
            this.lblPedido.Size = new System.Drawing.Size(51, 21);
            this.lblPedido.TabIndex = 4;
            this.lblPedido.Text = "Pedido";
            this.lblPedido.UseCompatibleTextRendering = true;
            this.lblPedido.Click += new System.EventHandler(this.label2_Click);
            // 
            // lblAlbaran
            // 
            this.lblAlbaran.AutoSize = true;
            this.lblAlbaran.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAlbaran.Location = new System.Drawing.Point(13, 142);
            this.lblAlbaran.Name = "lblAlbaran";
            this.lblAlbaran.Size = new System.Drawing.Size(59, 16);
            this.lblAlbaran.TabIndex = 5;
            this.lblAlbaran.Text = "Albaran";
            this.lblAlbaran.Click += new System.EventHandler(this.label3_Click);
            // 
            // txtDato1
            // 
            this.txtDato1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDato1.Location = new System.Drawing.Point(149, 68);
            this.txtDato1.Name = "txtDato1";
            this.txtDato1.Size = new System.Drawing.Size(171, 23);
            this.txtDato1.TabIndex = 6;
            this.txtDato1.TextChanged += new System.EventHandler(this.txtDato1_TextChanged);
            this.txtDato1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.txtDato1_MouseMove);
            // 
            // txtDato2
            // 
            this.txtDato2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDato2.Location = new System.Drawing.Point(149, 105);
            this.txtDato2.Name = "txtDato2";
            this.txtDato2.Size = new System.Drawing.Size(171, 23);
            this.txtDato2.TabIndex = 7;
            this.txtDato2.TextChanged += new System.EventHandler(this.txtDato2_TextChanged);
            this.txtDato2.MouseMove += new System.Windows.Forms.MouseEventHandler(this.txtDato2_MouseMove);
            // 
            // txtDato3
            // 
            this.txtDato3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDato3.Location = new System.Drawing.Point(149, 105);
            this.txtDato3.Name = "txtDato3";
            this.txtDato3.Size = new System.Drawing.Size(171, 23);
            this.txtDato3.TabIndex = 8;
            this.txtDato3.TextChanged += new System.EventHandler(this.txtDato3_TextChanged);
            this.txtDato3.MouseMove += new System.Windows.Forms.MouseEventHandler(this.txtDato3_MouseMove);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Tahoma", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(389, 101);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(86, 28);
            this.button2.TabIndex = 9;
            this.button2.Text = "Procesar";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "notifyIcon1";
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.Click += new System.EventHandler(this.notifyIcon1_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.Enabled = false;
            this.comboBox1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Pepsico",
            "Costco"});
            this.comboBox1.Location = new System.Drawing.Point(118, 11);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(171, 24);
            this.comboBox1.TabIndex = 10;
            this.comboBox1.Text = "PEPSICO";
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(13, 12);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 16);
            this.label4.TabIndex = 11;
            this.label4.Text = "Cliente";
            // 
            // lblAyuda
            // 
            this.lblAyuda.AutoSize = true;
            this.lblAyuda.Location = new System.Drawing.Point(336, 22);
            this.lblAyuda.Name = "lblAyuda";
            this.lblAyuda.Size = new System.Drawing.Size(37, 13);
            this.lblAyuda.TabIndex = 12;
            this.lblAyuda.Text = "Ayuda";
            // 
            // lblTipoDeProveedor
            // 
            this.lblTipoDeProveedor.AutoSize = true;
            this.lblTipoDeProveedor.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTipoDeProveedor.Location = new System.Drawing.Point(10, 71);
            this.lblTipoDeProveedor.Name = "lblTipoDeProveedor";
            this.lblTipoDeProveedor.Size = new System.Drawing.Size(130, 16);
            this.lblTipoDeProveedor.TabIndex = 13;
            this.lblTipoDeProveedor.Text = " Tipo de Proveedor";
            // 
            // comboBox2
            // 
            this.comboBox2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "Expenses"});
            this.comboBox2.Location = new System.Drawing.Point(149, 42);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(171, 24);
            this.comboBox2.TabIndex = 14;
            this.comboBox2.Text = "Seleccionar";
            // 
            // lblNoProveedor
            // 
            this.lblNoProveedor.AutoSize = true;
            this.lblNoProveedor.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNoProveedor.Location = new System.Drawing.Point(12, 142);
            this.lblNoProveedor.Name = "lblNoProveedor";
            this.lblNoProveedor.Size = new System.Drawing.Size(120, 16);
            this.lblNoProveedor.TabIndex = 15;
            this.lblNoProveedor.Text = "No. de proveedor";
            this.lblNoProveedor.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(13, 177);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(115, 16);
            this.label1.TabIndex = 16;
            this.label1.Text = "Termino de pago";
            // 
            // txtDato4
            // 
            this.txtDato4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDato4.Location = new System.Drawing.Point(149, 142);
            this.txtDato4.Name = "txtDato4";
            this.txtDato4.Size = new System.Drawing.Size(171, 23);
            this.txtDato4.TabIndex = 17;
            this.txtDato4.MouseMove += new System.Windows.Forms.MouseEventHandler(this.txtDato4_MouseMove);
            // 
            // lblCorreo
            // 
            this.lblCorreo.AutoSize = true;
            this.lblCorreo.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCorreo.Location = new System.Drawing.Point(265, 42);
            this.lblCorreo.Name = "lblCorreo";
            this.lblCorreo.Size = new System.Drawing.Size(52, 16);
            this.lblCorreo.TabIndex = 18;
            this.lblCorreo.Text = "Correo";
            // 
            // txtDato5
            // 
            this.txtDato5.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDato5.Location = new System.Drawing.Point(401, 39);
            this.txtDato5.Name = "txtDato5";
            this.txtDato5.Size = new System.Drawing.Size(171, 23);
            this.txtDato5.TabIndex = 19;
            this.txtDato5.MouseMove += new System.Windows.Forms.MouseEventHandler(this.txtDato5_MouseMove);
            // 
            // lblProveedor
            // 
            this.lblProveedor.AutoSize = true;
            this.lblProveedor.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProveedor.Location = new System.Drawing.Point(13, 74);
            this.lblProveedor.Name = "lblProveedor";
            this.lblProveedor.Size = new System.Drawing.Size(120, 16);
            this.lblProveedor.TabIndex = 20;
            this.lblProveedor.Text = "No. de proveedor";
            // 
            // lblPedidoP
            // 
            this.lblPedidoP.AutoSize = true;
            this.lblPedidoP.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPedidoP.Location = new System.Drawing.Point(13, 105);
            this.lblPedidoP.Name = "lblPedidoP";
            this.lblPedidoP.Size = new System.Drawing.Size(69, 16);
            this.lblPedidoP.TabIndex = 22;
            this.lblPedidoP.Text = "ID Pedido";
            // 
            // lblRecepcion
            // 
            this.lblRecepcion.AutoSize = true;
            this.lblRecepcion.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRecepcion.Location = new System.Drawing.Point(13, 142);
            this.lblRecepcion.Name = "lblRecepcion";
            this.lblRecepcion.Size = new System.Drawing.Size(92, 16);
            this.lblRecepcion.TabIndex = 23;
            this.lblRecepcion.Text = "ID Recepción";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(627, 243);
            this.Controls.Add(this.lblRecepcion);
            this.Controls.Add(this.lblPedidoP);
            this.Controls.Add(this.lblProveedor);
            this.Controls.Add(this.txtDato5);
            this.Controls.Add(this.lblCorreo);
            this.Controls.Add(this.txtDato4);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblNoProveedor);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.lblTipoDeProveedor);
            this.Controls.Add(this.lblAyuda);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.txtDato3);
            this.Controls.Add(this.txtDato2);
            this.Controls.Add(this.txtDato1);
            this.Controls.Add(this.lblAlbaran);
            this.Controls.Add(this.lblPedido);
            this.Controls.Add(this.lblTransaccion);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.ShowInTaskbar = false;
            this.Text = "Adendador";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Resize += new System.EventHandler(this.Form1_Resize);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label lblTransaccion;
        private System.Windows.Forms.Label lblPedido;
        private System.Windows.Forms.Label lblAlbaran;
        private System.Windows.Forms.TextBox txtDato1;
        private System.Windows.Forms.TextBox txtDato2;
        private System.Windows.Forms.TextBox txtDato3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblAyuda;
        private System.Windows.Forms.Label lblTipoDeProveedor;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Label lblNoProveedor;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtDato4;
        private System.Windows.Forms.Label lblCorreo;
        private System.Windows.Forms.TextBox txtDato5;
        private System.Windows.Forms.Label lblProveedor;
        private System.Windows.Forms.Label lblPedidoP;
        private System.Windows.Forms.Label lblRecepcion;
    }
}

