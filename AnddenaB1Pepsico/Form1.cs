﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace AddendaB1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            Notificacion();
            txtDato2.Visible = false;
            lblTipoDeProveedor.Visible = false;
            comboBox2.Visible = false;
            label1.Visible = false;
            lblCorreo.Visible = false;
            txtDato5.Visible = false;
            lblTransaccion.Visible = false;
            lblAlbaran.Visible = false;
            lblTipoDeProveedor.Visible = false;
            comboBox2.Visible = false;
            lblNoProveedor.Visible = false;
            lblPedido.Visible = false;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
        private void Form1_Resize(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            openFileDialog1.InitialDirectory = "C:\\";
            openFileDialog1.Filter = "Archivo XML (*.xml)|*.xml";

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    string str_RutaArchivo = openFileDialog1.FileName;
                    textBox1.Text = str_RutaArchivo;
                }
                catch (Exception)
                {

                }
            }
        }
        //Addenda de Pepsico
        private void GenerarPepsico()
        {
            if (txtDato1.TextLength!=0)
            {
                if (txtDato3.TextLength!=0)
                {
                    if (txtDato4.TextLength!=0)
                    {
                        if (textBox1.TextLength!=0)
                        {
                            #region
                            string[] datosXML = new string[6];
                            string xmlcantidad = string.Empty;
                            string xmlunidad = string.Empty;
                            string xmldescripcion = string.Empty;
                            string xmlvalorunitario = string.Empty;
                            string xmlImporte = string.Empty;
                            string xmlUUID = string.Empty;
                            #endregion
                            string No_Proveedor = txtDato1.Text;
                            string Pedido = txtDato3.Text;
                            string ID_Recepcion = txtDato4.Text;
                            string SolicitudPago = txtDato4.Text;
                            string dt = DateTime.Now.ToString("yyyy/MM/dd");
                            string strPathDestino = "C:\\Addendas\\Pepsico\\" + dt;
                            string strPath = textBox1.Text;
                            string strPath1 = textBox1.Text;
                            string nombrearchivo = Path.GetFileNameWithoutExtension(strPath);
                            FileInfo fi = new FileInfo(strPath);
                            if (!Directory.Exists(strPathDestino))
                            {
                                Directory.CreateDirectory(strPathDestino);
                            }
                            datosXML = obtenDatosXMLPepsico(strPath1);
                            if (datosXML!=null)
                            {
                                xmlcantidad = datosXML[0] == null ? null : datosXML[0].ToString();
                                xmlunidad = datosXML[1] == null ? null : datosXML[1].ToString();
                                xmldescripcion = datosXML[2] == null ? null : datosXML[2].ToString();
                                xmlvalorunitario = datosXML[3] == null ? null : datosXML[3].ToString();
                                xmlImporte = datosXML[3] == null ? null : datosXML[4].ToString();
                                xmlUUID = datosXML[4] == null ? null : datosXML[5].ToString();
                                string ruta = strPathDestino;
                                string destino = Path.Combine(ruta, nombrearchivo + ".txt");
                                fi.CopyTo(destino, true);
                                string text = File.ReadAllText(destino);
                                string text1 = text.Substring(0, text.IndexOf("</cfdi:Comprobante>"));
                                string addenda = "";

                                addenda += "<cfdi:Addenda>";
                                addenda += "<RequestCFD tipo='AddendaPCO' version='2.0' idPedido='" + Pedido + "'>";
                                addenda += "<Documento folioUUID='" + xmlUUID + "' tipoDoc='1' />";
                                addenda += "<Proveedor idProveedor='" + No_Proveedor + "' />";
                                addenda += "<Recepciones>";
                                addenda += "<Recepcion idRecepcion='"+ ID_Recepcion + "'>";
                                addenda += "<Concepto cantidad='" + xmlcantidad + "' unidad='SERVICIO' descripcion='" + xmldescripcion + "' valorUnitario='" + xmlvalorunitario + "' importe='" + xmlImporte + "' />";
                                addenda += "</Recepcion>";
                                addenda += "</Recepciones>";
                                addenda += "</RequestCFD>";
                                addenda += "</cfdi:Addenda>";
                                addenda += "</cfdi:Comprobante>";

                                string textF = text1 + addenda;
                                File.WriteAllText(destino, textF);
                                FileInfo fi1 = new FileInfo(destino);
                                string destino1 = Path.Combine(ruta, nombrearchivo + ".xml");
                                fi1.CopyTo(destino1, true);
                                File.Delete(destino);
                                MessageBox.Show("Se creo correctamente la Addenda " + destino1, "Creación Addenda", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                txtDato1.Text = "";
                                txtDato2.Text = "";
                                txtDato3.Text = "";
                                txtDato4.Text = "";
                                textBox1.Text = "";
                            }
                        }
                        else
                        {
                            MessageBox.Show("Seleccionar archivo valido", "Creación Addenda", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Ingrese ID Recepción", "Creación Addenda", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Ingrese ID Pedido", "Creación Addenda", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Ingrese No. de proveedor", "Creación Addenda", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //Addenda de Costco
        private void GenerarCostco()
        {
            if (comboBox2.SelectedIndex > -1)
            {
                if (txtDato2.TextLength != 0)
                {
                    if (txtDato3.TextLength != 0)
                    {
                        if (txtDato4.TextLength != 0)
                        {
                            if (txtDato5.TextLength != 0)
                            {
                                if (textBox1.TextLength != 0)
                                {
                                    #region datos XML
                                    string[] datosXML = new string[2];
                                    string xmlSerie = string.Empty;
                                    string xmlMoneda = string.Empty;
                                    #endregion
                                    string dt = DateTime.Now.ToString("yyyy/MM/dd");
                                    string strPathDestino = "C:\\Addendas\\Costco\\" + dt;
                                    string strPath = textBox1.Text;
                                    string strPath1 = textBox1.Text;
                                    string nombrearchivo = Path.GetFileNameWithoutExtension(strPath);
                                    object select1 = comboBox2.SelectedItem;
                                    string select2 = select1.ToString();
                                    string Tipo_proveedor = "";
                                    if (select2== "Expenses")
                                    {
                                        Tipo_proveedor = "E";
                                    }
                                    string Pedido = txtDato2.Text;
                                    string No_Proveedor = txtDato3.Text;
                                    string Termino_pago = txtDato4.Text;
                                    string correo = txtDato5.Text;
                                    int longi = No_Proveedor.Length;
                                    int res = longi - 2;
                                    string longitud = No_Proveedor.Substring(0,5);
                                    string longitud1 = No_Proveedor.Substring(res,2);
                                    FileInfo fi = new FileInfo(strPath);
                                    if (!Directory.Exists(strPathDestino))
                                    {
                                        Directory.CreateDirectory(strPathDestino);
                                    }
                                    datosXML = obtenDatosXMLCostco(strPath1);
                                    if (datosXML != null)
                                    {
                                        xmlSerie = datosXML[0] == null ? null : datosXML[0].ToString();
                                        xmlMoneda = datosXML[1] == null ? null : datosXML[1].ToString();
                                        string moneda = "";
                                        if (xmlMoneda=="MXN")
                                        {
                                            moneda = "MXP";
                                        }

                                        string ruta = strPathDestino;
                                        string destino = Path.Combine(ruta, nombrearchivo + ".txt");
                                        fi.CopyTo(destino, true);
                                        string text = File.ReadAllText(destino);
                                        string text1 = text.Substring(0, text.IndexOf("</cfdi:Comprobante>"));
                                        string addenda = "";

                                        addenda += "<cfdi:Addenda>";
                                        addenda += "<xs:Addenda_Costco_Detecno xsi:schemaLocation='http://www.w3.org/2001/XMLSchema/addenda/xs http://www.w3.org/2001/XMLSchema/addenda/xs/XS_Addenda.xsd' xmlns:xs='http://www.w3.org/2001/XMLSchema'>";
                                        addenda += "<xs:Version Version='1.0' />";
                                        addenda += "<xs:TipoProveedor>" + Tipo_proveedor + "</xs:TipoProveedor>";
                                        addenda += "<xs:NoProveedor>" + longitud + "</xs:NoProveedor>";
                                        addenda += "<xs:Subfijo>" + longitud1 + "</xs:Subfijo>";
                                        addenda += "<xs:NoOc>" + Pedido + "</xs:NoOc>";
                                        addenda += "<xs:Moneda>"+moneda+"</xs:Moneda>";
                                        addenda += "<xs:TerminosPago>" + Termino_pago + "</xs:TerminosPago>";
                                        addenda += "<xs:IDFactura>"+xmlSerie+"</xs:IDFactura>";
                                        addenda += "<xs:CorreoAviso>" + correo + "</xs:CorreoAviso>";
                                        addenda += "</xs:Addenda_Costco_Detecno>";
                                        addenda += "</cfdi:Addenda>";
                                        addenda += "</cfdi:Comprobante>";

                                        string textF = text1 + addenda;
                                        File.WriteAllText(destino, textF);
                                        FileInfo fi1 = new FileInfo(destino);
                                        string destino1 = Path.Combine(ruta, nombrearchivo + ".xml");
                                        fi1.CopyTo(destino1, true);
                                        File.Delete(destino);
                                        MessageBox.Show("Se creo correctamente la Addenda " + destino1, "Creación Addenda", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        txtDato2.Text = "";
                                        txtDato3.Text = "";
                                        txtDato4.Text = "";
                                        txtDato5.Text = "";
                                        comboBox2.SelectedIndex = -1;
                                        textBox1.Text = "";
                                    }
                                    else
                                    {
                                        MessageBox.Show("Datos de XML incorrectos", "Creación Addenda", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    }
                                    
                                }
                                else
                                {
                                    MessageBox.Show("Seleccione archivo XML", "Creación Addenda", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                }
                            }
                            else
                            {
                                MessageBox.Show("Ingrese un correo electronico", "Creación Addenda", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        else
                        {
                            MessageBox.Show("Ingrese un termino de pago", "Creación Addenda", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Ingrese un número de proveedor", "Creación Addenda", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Ingrese un ID de pedido", "Creación Addenda", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Elegir una opción", "Creación Addenda", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //Addenda de Envases Universales
        private void GenerarEnvases()
        {
            if (txtDato1.TextLength != 0)
            {
                if (txtDato2.TextLength != 0)
                {
                    if (txtDato3.TextLength != 0)
                    {
                        if (textBox1.TextLength != 0)
                        {

                            #region Variables para el archivo XML
                            string[] datosXML = new string[7];
                            DateTime xmlFfactura = new DateTime();
                            string xmlMoneda = string.Empty;
                            string xmlTipoCambio = string.Empty;
                            decimal xmlSubtotal = 0;
                            decimal xmlTotal = 0;
                            decimal xmlImpuesto = 0;
                            #endregion

                            string dt = DateTime.Now.ToString("yyyy/MM/dd");
                            string strPathDestino = "C:\\Addendas\\EnvasesUniversales\\" + dt;
                            string strPath = textBox1.Text;
                            string strPath1 = textBox1.Text;
                            string nombrearchivo = Path.GetFileNameWithoutExtension(strPath);
                            string transaccion = txtDato1.Text;
                            string IdPedido = txtDato2.Text;
                            string albaran = txtDato3.Text;
                            FileInfo fi = new FileInfo(strPath);
                            if (!Directory.Exists(strPathDestino))
                            {
                                Directory.CreateDirectory(strPathDestino);
                            }
                            datosXML = obtenDatosXML(strPath1);
                            if (datosXML != null)
                            {
                                xmlFfactura = datosXML[0] == null ? DateTime.Now : Convert.ToDateTime(datosXML[0].ToString());
                                xmlMoneda = datosXML[1] == null ? null : datosXML[1].ToString();
                                xmlTipoCambio = datosXML[2] == null ? null : datosXML[2].ToString();
                                xmlSubtotal = datosXML[3] == null ? 0 : Convert.ToDecimal(datosXML[3].ToString());
                                xmlTotal = datosXML[4] == null ? 0 : Convert.ToDecimal(datosXML[4].ToString());
                                xmlImpuesto = datosXML[5] == null ? 0 : Convert.ToDecimal(datosXML[5].ToString());
                                string fecha = Convert.ToString(xmlFfactura.ToString("yyyy-MM-dd"));

                                string ruta = strPathDestino;
                                string destino = Path.Combine(ruta, nombrearchivo + ".txt");
                                fi.CopyTo(destino, true);
                                string text = File.ReadAllText(destino);
                                string text1 = text.Substring(0, text.IndexOf("</cfdi:Comprobante>"));
                                string addenda = "";
                                addenda += "<cfdi:Addenda>";
                                addenda += "<eu:AddendaEU xsi:schemaLocation='http://factura.envasesuniversales.com/addenda/eu http://factura.envasesuniversales.com/addenda/eu/EU_Addenda.xsd' xmlns:eu='http://factura.envasesuniversales.com/addenda/eu'>";
                                addenda += "<eu:TipoFactura>";
                                addenda += "<eu:IdFactura>Factura</eu:IdFactura>";
                                addenda += "<eu:Version>1.0</eu:Version>";
                                addenda += "<eu:FechaMensaje>"+fecha+"</eu:FechaMensaje>";
                                addenda += "</eu:TipoFactura>";
                                addenda += "<eu:TipoTransaccion>";
                                addenda += "<eu:IdTransaccion>Con_Pedido</eu:IdTransaccion>";
                                addenda += "<eu:Transaccion>" + transaccion + "</eu:Transaccion>";
                                addenda += "</eu:TipoTransaccion>";
                                addenda += "<eu:OrdenesCompra>";
                                addenda += "<eu:Secuencia consec='1'>";
                                addenda += "<eu:IdPedido>" + IdPedido + "</eu:IdPedido>";
                                addenda += "<eu:EntradaAlmacen>";
                                addenda += "<eu:Albaran>" + albaran + "</eu:Albaran>";
                                addenda += "</eu:EntradaAlmacen>";
                                addenda += "</eu:Secuencia>";
                                addenda += "</eu:OrdenesCompra>";
                                addenda += "<eu:Moneda>";
                                addenda += "<eu:MonedaCve>"+xmlMoneda+"</eu:MonedaCve>";
                                addenda += "<eu:TipoCambio>"+xmlTipoCambio+".000000</eu:TipoCambio>";
                                addenda += "<eu:SubtotalM>"+xmlSubtotal+"</eu:SubtotalM>";
                                addenda += "<eu:TotalM>"+xmlTotal+"</eu:TotalM>";
                                addenda += "<eu:ImpuestoM>"+xmlImpuesto+"</eu:ImpuestoM>";
                                addenda += "</eu:Moneda>";
                                addenda += "</eu:AddendaEU>";
                                addenda += "</cfdi:Addenda>";
                                addenda += "</cfdi:Comprobante>";

                                string textF = text1 + addenda;

                                File.WriteAllText(destino, textF);
                                FileInfo fi1 = new FileInfo(destino);
                                string destino1 = Path.Combine(ruta, nombrearchivo + ".xml");
                                fi1.CopyTo(destino1, true);
                                File.Delete(destino);
                                MessageBox.Show("Se creo correctamente la Addenda " + destino1, "Creación Addenda", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                txtDato1.Text = "";
                                txtDato2.Text = "";
                                txtDato3.Text = "";
                                textBox1.Text = "";
                            }
                            else
                            {
                                MessageBox.Show("Factura no valida", "Creación Addenda", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        else
                        {
                            MessageBox.Show("Seleccionar archivo", "Creación Addenda", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Es necesario capturar Albaran", "Creación Addenda", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Es necesario capturar el Id del Pedido", "Creación Addenda", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Es necesario capturar la Transacción", "Creación Addenda", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        
        //E = expenses 
        //M = Merchandise

        private void button2_Click(object sender, EventArgs e)
        {
            GenerarPepsico();
          //GenerarCostco();
          //GenerarEnvases();
        }
        
        public void Notificacion()
        {
            notifyIcon1.Text = "Creación addenda";
            notifyIcon1.BalloonTipTitle = "Creación Addenda";
            notifyIcon1.BalloonTipText = "Creación de addenda";
            notifyIcon1.BalloonTipIcon = ToolTipIcon.Info;

            notifyIcon1.Visible = true;

            notifyIcon1.ShowBalloonTip(50000);
        }

        private void notifyIcon1_Click(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Normal)
            {
                this.WindowState = FormWindowState.Minimized;
                ShowInTaskbar = false;
            }
            else
            {
                this.WindowState = FormWindowState.Normal;
                ShowInTaskbar = false;
            }
        }
        private string[] validaArchivos(string path)
        {
            string resp = "";
            string[] returResp = new string[3];
            int noXML = 0;

            string nom = Path.GetDirectoryName(path);
            string nombreXML = Path.GetFileName(path);
            string ext = Path.GetExtension(nombreXML);

            if (Directory.Exists(nom))
            {
                if (File.Exists(path))
                {
                    if (ext==".xml")
                    {
                        if (resp == "")
                        {
                            returResp[0] = noXML.ToString();
                            returResp[1] = nombreXML.ToString();
                            returResp[2] = "";
                        }
                        else
                        {
                            returResp[0] = noXML.ToString();
                            returResp[1] = resp;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Extensión de archivo no valida", "Creación Addenda", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("El archivo no existe", "Creación Addenda", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("No existe el directorio", "Creación Addenda", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return returResp;
        }
        private string[] obtenDatosXMLCostco(string path)
        {
            string[] arrRespuesta = new string[2];
            XmlDocument xmlFactura = new XmlDocument();
            try
            {
                xmlFactura.Load(path);

                XmlNode nodoFactura = xmlFactura.DocumentElement;

                XmlAttribute rootAtributo = nodoFactura.Attributes["Folio"];
                if (rootAtributo != null)
                    arrRespuesta[0] = rootAtributo.Value.ToString();

                rootAtributo = null;
                rootAtributo = nodoFactura.Attributes["Moneda"];
                if (rootAtributo != null)
                    arrRespuesta[1] = rootAtributo.Value.ToString();
                xmlFactura.Clone();
            }
            catch (Exception)
            {
                arrRespuesta = null;
            }

            return arrRespuesta;
        }
        private string[] obtenDatosXML(string path)
        {
            string[] arrRespuesta = new string[7];

            //Documento XML
            XmlDocument xmlFactura = new XmlDocument();
            try
            {
                xmlFactura.Load(path);
                //Nodos del documento
                XmlNode nodoFactura = xmlFactura.DocumentElement;

                //Atributos Root
                XmlAttribute rootAtributo = nodoFactura.Attributes["Fecha"];
                if (rootAtributo != null)
                    arrRespuesta[0] = rootAtributo.Value.ToString();

                rootAtributo = null;
                rootAtributo = nodoFactura.Attributes["Moneda"];
                if (rootAtributo != null)
                    arrRespuesta[1] = rootAtributo.Value.ToString();

                rootAtributo = null;
                rootAtributo = nodoFactura.Attributes["TipoCambio"];
                if (rootAtributo != null)
                    arrRespuesta[2] = rootAtributo.Value.ToString();


                rootAtributo = null;
                rootAtributo = nodoFactura.Attributes["SubTotal"];
                if (rootAtributo != null)
                    arrRespuesta[3] = rootAtributo.Value.ToString();

                rootAtributo = null;
                rootAtributo = nodoFactura.Attributes["Total"];
                if (rootAtributo != null)
                    arrRespuesta[4] = rootAtributo.Value.ToString();

                XmlNodeList nodeList = xmlFactura.GetElementsByTagName("cfdi:Impuestos");
                foreach (XmlElement nodo in nodeList)
                {
                    XmlNodeList nodeList1 = xmlFactura.GetElementsByTagName("cfdi:Traslados");
                    foreach (XmlElement nodo1 in nodeList1)
                    {
                        XmlNodeList nodeList2 = nodo1.GetElementsByTagName("cfdi:Traslado");
                        foreach (XmlElement nodo2 in nodeList2)
                        {
                            //XmlNodeList nodeListT = nodo2.GetElementsByTagName("cfdi:Traslados");
                            //foreach (XmlElement nodo3 in nodeListT)
                            //{
                            //    XmlNodeList nodeList4 = nodo3.GetElementsByTagName("cfdi:Traslado");
                            //    foreach (XmlElement nodo4 in nodeList4)
                            //    {
                                    arrRespuesta[5] = Convert.ToString(nodo2.GetAttribute("Importe"));
                                    //arrRespuesta[5] += Convert.ToDecimal(nodo2.GetAttribute("Importe").ToString());
                                //}
                            //}
                        }
                    }
                }

                xmlFactura.Clone();
            }
            catch (Exception ex)
            {

                arrRespuesta = null;
            }

            return arrRespuesta;

        }
        private string[] obtenDatosXMLPepsico(string path)
        {
            string[] arrRespuesta = new string[6];
            XmlDocument xmlFactura = new XmlDocument();
            try
            {
                xmlFactura.Load(path);
                XmlNodeList nodeList = xmlFactura.GetElementsByTagName("cfdi:Conceptos");
                foreach (XmlElement nodo in nodeList)
                {
                    XmlNodeList nodeList1 = xmlFactura.GetElementsByTagName("cfdi:Concepto");
                    foreach (XmlElement nodo1 in nodeList1)
                    {
                            arrRespuesta[0] = Convert.ToString(nodo1.GetAttribute("Cantidad"));
                            arrRespuesta[1] = Convert.ToString(nodo1.GetAttribute("ClaveUnidad"));
                            arrRespuesta[2] = Convert.ToString(nodo1.GetAttribute("Descripcion"));
                            arrRespuesta[3] = Convert.ToString(nodo1.GetAttribute("ValorUnitario"));
                            arrRespuesta[4] = Convert.ToString(nodo1.GetAttribute("Importe"));       
                    }
                }
                XmlNodeList nodeList2 = xmlFactura.GetElementsByTagName("tfd:TimbreFiscalDigital");
                foreach (XmlElement nodo2 in nodeList2)
                {
                    arrRespuesta[5] = Convert.ToString(nodo2.GetAttribute("UUID"));
                }
            }
            catch (Exception)
            {

                arrRespuesta = null;
            }
            
            
            xmlFactura.Clone();
            return arrRespuesta;
        }

        private void txtDato1_MouseMove(object sender, MouseEventArgs e)
        {
            lblAyuda.Text = "Se utiliza para especificar el identificador o número \n que la Sociedad (Empresa) de PEPSICO ha \n asignado al proveedor.";
            //lblAyuda.Text = "Número de Nota de entrega, Remisión, Carta Porte,\n etc. Con el que fue entregada la mercancia o servico \n (proveedor)";
        }

        private void txtDato2_MouseMove(object sender, MouseEventArgs e)
        {
            lblAyuda.Text = "Se utiliza para indicar el número de folio fiscal \n del comprobante, en caso de que se emitan CFDIs.";
            //lblAyuda.Text = "Es el número de Orden de Compra relacionada a la \n adquisición de su producto mismo que debió ser entregado \n a usted de manera oportuna.";
            //lblAyuda.Text = "Número de  pedido (EUM) a la que hace referencia \n la factura, longitud de 10";
        }

        private void txtDato3_MouseMove(object sender, MouseEventArgs e)
        {
            lblAyuda.Text = "Se utiliza para indicar el número de pedido u \n orden de compra, \n proporcionado por PEPSICO, asociado al \n comprobante (CFD / CFDI).";
            //lblAyuda.Text = "Es el número de proveedor asignado a \n usted por parte de Costco México.";
            //lblAyuda.Text = "Folio que se genera al darle entrada al material y/o servicio \n por parte de los almacenes  y/o Liberacion de \n embarques para transportistas, longitud de 10";
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            int select = comboBox1.SelectedIndex;
            object select1 = comboBox1.SelectedItem;
            string select2 = select1.ToString();
            if (select2 == "Pepsico")
            {
                MessageBox.Show("Hola"+select2);
            }
            if (select2 == "Costco")
            {
                MessageBox.Show("Hola" + select2);

            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void txtDato1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void txtDato2_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtDato3_TextChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void txtDato4_MouseMove(object sender, MouseEventArgs e)
        {
            lblAyuda.Text = "Se utiliza para especificar el número con el \n cuál entra la mercancía al almacén. Este \n dato es proporcionado \n por PEPSICO al proveedor.";
            //lblAyuda.Text = "Es un campo que solamente aplica para los proveedores de \n tipo expenses (E) en donde deberá indicar los \n términos de pago pactados con su comprador en \n Costo México.";
        }

        private void txtDato5_MouseMove(object sender, MouseEventArgs e)
        {
            lblAyuda.Text = "Deberá indicar la cuenta de correo electrónico \n de su contacto en Costco México para \n realizar el proceso de pago.";
        }
    }
}
